<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Nico Heulsen
 * Date: 16/08/12
 * Time: 20:17
 */

/**
 * Returns the metriweb settings form
 *
 * @param $form_state
 */
function metriweb_settings_form(&$form_state) {
  $form = array();

  $form['general'] = array(
    '#type' => 'fieldset',
    '#title' => t('General'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['general']['metriweb_settings_sitename'] = array(
    '#type' => 'textfield',
    '#title' => t('Sitename'),
    '#description' => t('Single sitename used in the entire website. (Should be approved by CIM).'),
    '#default_value' => variable_get('metriweb_settings_sitename', ''),
  );

  $form['general']['metriweb_settings_language'] = array(
    '#type' => 'radios',
    '#title' => t('Language'),
    '#description' => t('Select the language of the website. (Not yet used in reports).'),
    '#options' => array('NL' => t('Dutch'), 'FR' => t('French'), 'EN' => t('English'), 'GE' => t('German')),
    '#default_value' => variable_get('metriweb_settings_language', ''),
  );

  $form['node_types'] = array(
    '#type' => 'fieldset',
    '#title' => t('Node types'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['node_types']['metriweb_settings_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => '',
    '#description' => t('When selected the content path of metriweb tagging can be overridden on a per node base.'),
    '#options' => node_get_types('names'),
    '#default_value' => variable_get('metriweb_settings_node_types', array()),
  );

  $form['content_path'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content path'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['content_path']['metriweb_settings_publication_unit_site'] = array(
    '#type' => 'textfield',
    '#title' => t('Site <em>(si_) </em>'),
    '#description' => t('Single sitename tag used as default (no sitesection, cobranded part, ...)'),
    '#default_value' => variable_get('metriweb_settings_publication_unit_site', ''),
  );

  $form['content_path']['metriweb_settings_publication_unit_site_sections'] = array(
    '#type' => 'textarea',
    '#title' => t('Site section <em>(ss_) </em>'),
    '#description' => t('Add all sitesection, be aware that these should be approved by CIM and the key should be prefixed with "ss_".  Each line represent a sitesection.  You can add key/value pair like (KEY|VALUE)'),
    '#default_value' => variable_get('metriweb_settings_publication_unit_site_sections', ''),
  );

  $form['content_path']['metriweb_settings_publication_unit_cobranded_sections'] = array(
    '#type' => 'textarea',
    '#title' => t('Cobranded section <em>(cb_) </em>'),
    '#description' => t('Add all cobranded section, be aware that these should be approved by CIM and the key should be prefixed with "cb_".  Each line represent a cobranded section.  You can add key/value pair like (KEY|VALUE)'),
    '#default_value' => variable_get('metriweb_settings_publication_unit_cobranded_sections', ''),
  );

  $form['content_path']['metriweb_settings_publication_unit_application_sites'] = array(
    '#type' => 'textarea',
    '#title' => t('Application site <em>(asi_) </em>'),
    '#description' => t('Add all application sites, be aware that these should be approved by CIM and the key should be prefixed with "asi_".  Each line represent a application site.  You can add key/value pair like (KEY|VALUE)'),
    '#default_value' => variable_get('metriweb_settings_publication_unit_application_sites', ''),
  );

  $form['content_path']['metriweb_settings_publication_unit_application_sections'] = array(
    '#type' => 'textarea',
    '#title' => t('Application sections <em>(ass_) </em>'),
    '#description' => t('Add all application sections, be aware that these should be approved by CIM and the key should be prefixed with "ass_".  Each line represent a application site section.  You can add key/value pair like (KEY|VALUE)'),
    '#default_value' => variable_get('metriweb_settings_publication_unit_application_sections', ''),
  );

  $form['content_path']['metriweb_settings_content_types'] = array(
    '#type' => 'textarea',
    '#title' => t('Content types'),
    '#description' => t('Content types, be aware that these should be approved by CIM.  Each line represent a cobranded section.  You can add key/value pair like (KEY|VALUE)'),
    '#default_value' => variable_get('metriweb_settings_content_types', ''),
  );

  $form['exclude'] = array(
    '#type' => 'fieldset',
    '#title' => t('Exclude'),
    '#description' => t('Exclude metriweb tagging by path or role.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['exclude']['metriweb_settings_exclude_paths'] = array(
    '#type' => 'textarea',
    '#title' => t('Exclude paths'),
    '#description' => t('Enter one page per line as Drupal paths. The "*" can be used as wildcard. Example paths are %blog for the blog page and %admin-wildcard for every admin page. %front is the default front page.", array("%blog" => "blog", "%admin-wildcard" => "admin/*", "%front" => "<front>")'),
    '#default_value' => variable_get('metriweb_settings_exclude_paths', "admin*\nnode/add*"),
  );

  $form['exclude']['metriweb_settings_exclude_roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Exclude by roles'),
    '#description' => t('Select the roles that should excluded. When logged in with a selected role, CIM tagging will be disabled.'),
    '#options' => user_roles(),
    '#default_value' => variable_get('metriweb_settings_exclude_roles', array()),
  );






  return system_settings_form($form);
}
