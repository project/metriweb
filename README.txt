Description
-----------
CIM Metriweb module allows you to integrate with your website.


Requirements
------------
This module requires Drupal 6-x.

This module also require some other modules in orde to work properly
 - pathauto (to generate content-path based on path url)
 - libraries (to seperate the CIM Metriweb javascript and drupal code)
 - transliteration (not required but recommended).


Installation
------------
1) Copy/upload the metriweb module folder to the sites/all/modules/contrib
directory of your Drupal installation.

2) Download the spring.js into sites/all/libraries/metriweb.

3) Enable the Administration theme module in Drupal (Administer -> Modules).

4) Configure the module.


Configuration
-------------
CIM Metriweb toolkit can be configured at :
  Administer -> Site configuration -> Metriweb

Developers
----------
You can alter the parameters of the CIM Metriweb before it is writed to the page.


Author
------
Nico Heulsen <info@artx.be>
http://artx.be
