<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Nico Heulsen
 * Date: 16/08/12
 * Time: 20:06
 */

/**
 * Implementation of hook_perm
 *
 * @return array
 */
function metriweb_perm() {
  return array(
    'administer metriweb'
  );
}

/**
 * Implementation of hook_menu
 *
 * @return array
 */
function metriweb_menu() {
  $items['admin/settings/metriweb'] = array(
    'title' => 'Metriweb',
    'description' => 'Manage all metriweb settings and behaviour',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('metriweb_settings_form'),
    'access arguments' => array('administer metriweb'),
    'file' => 'includes/metriweb.admin.inc',
  );

  return $items;
}

/**
 * Implementation of hook_init.
 */
function metriweb_init() {
  drupal_add_js(libraries_get_path('metriweb') .'/spring.js');
}

/**
 * Implements hook_form_alter
 *
 * @param $form
 * @param $form_state
 * @param $form_id
 */
function metriweb_form_alter(&$form, &$form_state, $form_id) {
  if (isset($form['type']) && isset($form['#node']) && $form['type']['#value'] . '_node_form' == $form_id) {
    $node = $form['#node'];
    if (!empty($node->type) && metriweb_check_node_type_form($node->type)) {
      $form['metriweb'] = array(
        '#type' => 'fieldset',
        '#title' => t('Metriweb'),
        '#collapsible' => TRUE,
        '#collasped' => TRUE,
      );

      $form['metriweb']['status'] = array(
        '#type' => 'checkbox',
        '#title' => t('Override CIM tagging'),
        '#description' => t('When selected the CIM tagging will be overridden with the given params below for this node.'),
        '#default_value' => !empty($node->metriweb['status']) ? $node->metriweb['status'] : '',
      );

      $form['metriweb']['publication_unit'] = array(
        '#type' => 'select',
        '#title' => t('Publication unit'),
        '#description' => t('Select the appropriate publication unit for this node:<br /><ul><li>si_: Site</li><li>ss_: Site section</li><li>cb_: Cobranded section</li><li>asi_: Application site</li><li>ass_: Application site section</li></ul>'),
        '#options' => metriweb_fetch_publication_unit_options(),
        '#default_value' => !empty($node->metriweb['publication_unit']) ? $node->metriweb['publication_unit'] : '',
      );

      $form['metriweb']['content_types'] = array(
        '#type' => 'select',
        '#title' => t('Content type'),
        '#description' => t('Select the appropriate content type for this node.'),
        '#options' => metriweb_fetch_content_types_options(),
        '#default_value' => !empty($node->metriweb['content_type']) ? $node->metriweb['content_type'] : '',
      );

      $form['metriweb']['content_path_level_3'] = array(
        '#type' => 'textfield',
        '#title' => t('Free keyword (level 3)'),
        '#description' => t('You can add a keyword in here.'),
        '#default_value' => !empty($node->metriweb['content_path_level_3']) ? $node->metriweb['content_path_level_3'] : '',
      );

      $form['metriweb']['content_path_level_4'] = array(
        '#type' => 'textfield',
        '#title' => t('Free keyword (level 4)'),
        '#description' => t('You can add a keyword in here.'),
        '#default_value' => !empty($node->metriweb['content_path_level_4']) ? $node->metriweb['content_path_level_4'] : '',
      );

      $form['metriweb']['content_path_level_5'] = array(
        '#type' => 'textfield',
        '#title' => t('Page name (level 5)'),
        '#description' => t('You can add the page name in here.  If left empty, a name will be generated for you.'),
        '#default_value' => !empty($node->metriweb['content_path_level_5']) ? $node->metriweb['content_path_level_5'] : '',
      );
    }
  }
}

/**
 * Implementation of hook_footer
 */
function metriweb_footer() {
  $write_metriweb_tag = metriweb_check_exclude();
  if ($write_metriweb_tag) {
    $params = array(
      'sitename' => variable_get('metriweb_settings_sitename', ''),
      'content_path' => metriweb_fetch_content_path(),
      'language' => variable_get('metriweb_settings_language', ''),
      'url' => 'document.location.href',
    );

    drupal_alter('metriweb_params', $params);

    $js = '';

    $js .= "var sp_e0 = {\n";
    $js .= ' "s":"'. $params['sitename'] .'",' ."\n";
    $js .= ' "cp":"'. metriweb_build_content_path_string($params['content_path']) .'",' ."\n";
    $js .= ' "lang":"'. $params['language'] .'",' ."\n";
    $js .= ' "url": '. $params['url'] ."\n";
    $js .= "};\n";
    $js .= "\n";
    $js .= "spring.c(sp_e0);\n";

    drupal_add_js($js, 'inline', 'footer', FALSE, FALSE);
  }
}

/**
 * Fetch the content path based on several settings
 * and page variables.
 *
 * @return string
 */
function metriweb_fetch_content_path() {
  $cp = array();

  $cp['publication_unit'] = '';
  $cp['content_type'] = '';
  $cp['content_path_level_3'] = '';
  $cp['content_path_level_4'] = '';
  $cp['content_path_level_5'] = '';

  return $cp;
}

/**
 * Builds a content_path string based on given settings
 *
 * @param $raw
 * @return string
 */
function metriweb_build_content_path_string($raw) {
  $cp = '';
  foreach((array)$raw as $item) {
    if (!empty($item)) {
      $cp .= $item .'/';
    }
  }

  $cp = substr($cp, -1, 1) == '/' ? substr($cp, 0, -1) : $cp;

  return $cp;
}

/**
 * Check if metriweb should be executed in this context
 *
 * @return bool
 */
function metriweb_check_exclude() {
  global $user;

  $excluded_user_roles = variable_get('metriweb_settings_exclude_roles', array());
  $excluded_pages = variable_get('metriweb_settings_exclude_paths', '');

  foreach ((array)$user->roles as $key => $role) {
    if (in_array($key, array_values($excluded_user_roles))) {
      return FALSE;
    }
  }

  if (!empty($excluded_pages)) {
    if (drupal_match_path($_GET['q'], $excluded_pages)) {
      return FALSE;
    }

    $alias = drupal_get_path_alias($_GET['q']);
    if ($alias != $_GET['q']) {
      if (drupal_match_path($alias, $excluded_pages)) {
        return FALSE;
      }
    }
  }

  return TRUE;
}

/**
 * Check if metriweb params can be overridden for this node-type
 *
 * @param $node_type
 * @return bool
 */
function metriweb_check_node_type_form($node_type) {
  $node_types = variable_get('metriweb_settings_node_types', array());
  foreach ((array)array_values($node_types) as $type) {
    if (strtolower($type) == $node_type) {
      return TRUE;
    }
  }

  return FALSE;
}

/**
 * Returns an array of key/value pairs of all default publication unit options
 *
 * @return array
 */
function metriweb_fetch_publication_unit_options() {
  $options = array();
  $data = array(
    'si' => variable_get('metriweb_settings_publication_unit_site', ''),
    'ss' => variable_get('metriweb_settings_publication_unit_site_sections', ''),
    'cb' => variable_get('metriweb_settings_publication_unit_cobranded_sections', ''),
    'asi' => variable_get('metriweb_settings_publication_unit_application_sites', ''),
    'ass' => variable_get('metriweb_settings_publication_unit_application_sections', ''),
  );

  foreach ($data as $prefix => $raw) {
    $items = metriweb_build_array_from_string($raw, $prefix .'_');
    $options = array_merge($options, $items);
  }

  return $options;
}

/**
 * Returns an array of key/value pairs of all content type options
 *
 * @return array
 */
function metriweb_fetch_content_types_options() {
  $content_types = variable_get('metriweb_settings_content_types', '');
  return metriweb_build_array_from_string($content_types, '');
}

/**
 * Returs an array with key/value pairs based on a string (setting)
 *
 * @param $string
 * @param $prefix
 * @param string $title
 * @return array
 */
function metriweb_build_array_from_string($string, $prefix) {
  $items = array();
  $arr = explode("\n", $string);

  if (!empty($string)) {
    foreach ((array)$arr as $raw) {
      $item = explode("|", $raw);
      if (!empty($item) && is_array($item)) {
        $key = !empty($item[0]) && substr($item[0], 0, strlen(trim($prefix))) == $prefix ? trim($item[0]) : $prefix . trim($item[0]);
        if (sizeof($item) > 1) {
          $items[$key] = !empty($prefix) ? trim($item[1]) . ' ('. $prefix .')' : trim($item[1]);
        }
        else{
          if ($key != $prefix) {
            $items[$key] = !empty($prefix) ? ucfirst(substr($key, strlen($prefix)) .' ('. $prefix .')') : ucfirst(substr($key, strlen($prefix)));
          }
        }
      }
    }
  }

  return $items;
}
